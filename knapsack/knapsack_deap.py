from deap import tools, base, creator, algorithms
import random as rnd
import numpy as np
# from function_opt.ga_scheme import eaMuPlusLambda

creator.create("Fitness", base.Fitness, weights=(1.0, ))
creator.create("Backpack", set, fitness=creator.Fitness)

class Item:
    def __init__(self, weight, value):
        self.weight = weight
        self.value = value


class KnapsackProblem:
    def factory(self):
        # create individ
        backpack = set()
        for _ in range(self.initial_size):
            backpack.add(rnd.choice(self.items))
        backpack = creator.Backpack(backpack)
        return backpack

    def fitness(self, backpack):
        weight = 0.0
        value = 0.0
        for item in backpack:
            weight += item.weight
            value += item.value
        if weight > self.backpack_weight:
            value = 0.0
        return value,

    def mutation(self, mutant):
        if rnd.random() < 0.5:
            # add
            possible_items = [item for item in self.items if item not in mutant]
            mutant.add(rnd.choice(possible_items))
        else:
            if len(mutant) > 1:
                mutant.remove(rnd.choice(list(mutant)))
            # remove
        return mutant,

    def crossover(self, b1, b2):
        c1 = b1 & b2 # intersection
        c2 = b1 ^ b2 # symmetric difference
        if len(c1) == 0:
            c1.add(b1.pop())
        if len(c2) == 0:
            c2.add(b2.pop())
        return creator.Backpack(c1), creator.Backpack(c2)


    def __init__(self, backpack_weight, items):
        self.backpack_weight = backpack_weight
        self.items = items

        # factory
        self.initial_size = 10

        self.engine = base.Toolbox()
        self.engine.register("individual", self.factory)
        self.engine.register("population", tools.initRepeat, list, self.engine.individual)
        self.engine.register("mutate", self.mutation)
        self.engine.register("mate", self.crossover)
        self.engine.register("select", tools.selNSGA2)
        self.engine.register("evaluate", self.fitness)

    def run(self):
        pop_size = 50
        _lambda = 40
        iterations = 500

        pop = self.engine.population(pop_size)
        hof = tools.HallOfFame(3)
        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", np.mean, axis=0)
        stats.register("std", np.std, axis=0)
        stats.register("min", np.min, axis=0)
        stats.register("max", np.max, axis=0)

        algorithms.eaMuPlusLambda(pop, self.engine, mu=pop_size, lambda_=_lambda,
                                  cxpb=0.3, mutpb=0.4, stats=stats, halloffame=hof, ngen=iterations)

        return pop, stats, hof


if __name__ == "__main__":
    backpack_weight = 200.0
    items = [Item(rnd.randint(1, 10), rnd.randint(0, 50)) for i in range(50)]
    problem = KnapsackProblem(backpack_weight, items)
    pop, stats, hof = problem.run()

    for ind in hof:
        print("Ind value={} items={}".format(ind.fitness.values[0], len(ind)))
    best = hof[0]
    print(sum([item.weight for item in best]))