from deap import tools, base
from function_opt.ga_scheme import eaMuPlusLambda
from numpy import random as rnd
import numpy as np
from deap import creator

from function_opt.functions import rastrigin
from function_opt.draw_log import draw_log

creator.create("BaseFitness", base.Fitness, weights=(1.0,))
creator.create("Individual", np.ndarray, fitness=creator.BaseFitness)


def mutation(individual, mut_prob):
    n = len(individual)
    for i in range(n):
        if rnd.random() < mut_prob:
            swap_idx = rnd.randint(0, n - 1)
            individual[swap_idx], individual[i] = individual[i], individual[swap_idx]
    return individual,


class SimpleGAExperiment:
    def factory(self):
        return rnd.random(self.dimension) * 10 - 5

    def __init__(self, function, dimension, pop_size, iterations):
        self.pop_size = pop_size
        self.iterations = iterations
        self.mut_prob = 0.7
        self.cross_prob = 0.6

        self.function = function
        self.dimension = dimension

        self.engine = base.Toolbox()
        self.engine.register("map", map)
        self.engine.register("individual", tools.initIterate, creator.Individual, self.factory)
        self.engine.register("population", tools.initRepeat, list, self.engine.individual, self.pop_size)
        self.engine.register("mate", tools.cxOnePoint)
        self.engine.register("mutate", mutation)
        self.engine.register("select", tools.selTournament, tournsize=4)
        self.engine.register("evaluate", self.function)

    def run(self):
        pop = self.engine.population()

        hof = tools.HallOfFame(5, np.array_equal)
        stats = tools.Statistics(lambda ind: ind.fitness.values[0])
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)

        pop, log = eaMuPlusLambda(pop, self.engine, mu=self.pop_size, lambda_=int(self.pop_size * 0.8),
                                  cxpb=self.cross_prob, mutpb=self.mut_prob,
                                  ngen=self.iterations,
                                  stats=stats, halloffame=hof, verbose=True)
        print("Best = {}".format(hof[0]))
        print("Best fit = {}".format(hof[0].fitness.values[0]))
        return log


if __name__ == "__main__":
    def function(x):
        res = rastrigin(x)
        return res,


    dimension = 100
    pop_size = 100
    iterations = 300
    scenario = SimpleGAExperiment(function, dimension, pop_size, iterations)
    log = scenario.run()
    draw_log(log)
